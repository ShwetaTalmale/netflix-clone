//
//  DownloadsViewController.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 07/01/23.
//

import UIKit

class DownloadsViewController: UIViewController {
    // MARK: - Properties
    private let downloadTable: UITableView = {
       let tableView = UITableView()
        tableView.register(TitleTableViewCell.self, forCellReuseIdentifier: TitleTableViewCell.identifier)
        return tableView
    }()

    private let editButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Edit", for: .normal)
        button.setTitleColor(.link, for: .normal)
        return button
    }()

    private var titles: [TitleItem] = [TitleItem]()

    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(downloadTable)
        configureNavBar()
       // downloadTable.isEditing = true
        downloadTable.delegate = self
        downloadTable.dataSource = self
        fetchLocalstorageForDownload()
        NotificationCenter.default.addObserver(forName: NSNotification.Name("Downloaded"),
                                               object: nil, queue: nil) { _ in
            self.fetchLocalstorageForDownload()
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        downloadTable.frame = view.bounds
    }

    private func configureNavBar() {
        title = "Downloads"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
        view.backgroundColor = .systemBackground
        editButton.addTarget(self, action: #selector(didTapEditButton), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: editButton)
    }

    @objc private func didTapEditButton() {
        downloadTable.isEditing = !downloadTable.isEditing
        if downloadTable.isEditing {
            editButton.setTitle("Done", for: .normal)
            editButton.setTitleColor(.red, for: .normal)
        } else {
            editButton.setTitle("Edit", for: .normal)
            editButton.setTitleColor(.link, for: .normal)
        }
        downloadTable.reloadData()
    }

    private func fetchLocalstorageForDownload() {
        DataPersistanceManager.shared.fetchingTitleFromDatabase { [weak self] result in
            switch result {
            case .success(let titleItems):
                self?.titles = titleItems
                DispatchQueue.main.async {
                    self?.downloadTable.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension DownloadsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TitleTableViewCell.identifier,
                                                       for: indexPath) as? TitleTableViewCell else {
            return UITableViewCell()
        }
        let title = titles[indexPath.row]
        cell.configure(with: TitleViewModel(titleName: title.title ?? title.original_title ?? "Unknown",
                                            posterUrl: title.poster_path ?? ""))
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let title = titles[indexPath.row]
        guard let titleName = title.title ?? title.original_title else { return}
        APICaller.shared.getMovie(with: titleName) { [weak self] result in
            switch result {
            case .success(let videoElement):
                DispatchQueue.main.async {
                    let vc = TitlePreviewViewController()
                    let movieTitle = Title(id: Int(title.id),
                                           media_type: title.media_type,
                                           original_language: title.original_language,
                                           original_title: title.original_title,
                                           overview: title.overview,
                                           popularity: CGFloat(title.popularity),
                                           poster_path: title.poster_path,
                                           release_date: title.release_date,
                                           title: title.title,
                                           vote_count: Int(title.vote_count))
                    vc.configure(with: TitlePreviewModel(title: titleName,
                                                             youTubeView: videoElement,
                                                             titleOverView: title.overview ?? ""),
                                 title: movieTitle)
                    self?.navigationController?.pushViewController(vc, animated: true)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

// MARK: - Table cell reordering and deleting
extension DownloadsViewController {
    func tableView(_ tableView: UITableView,
                   canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath,
                   to destinationIndexPath: IndexPath) {
        let selectedItem = titles[sourceIndexPath.row]
        titles.remove(at: sourceIndexPath.row)
        titles.insert(selectedItem, at: destinationIndexPath.row)
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            DataPersistanceManager.shared.deleteTitleWith(model: titles[indexPath.row]) { [weak self] result in
                switch result {
                case .success():
                    print("Deleted row")
                    self?.titles.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        default: break
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.subviews.forEach {
            if $0.description.range(of: "UITableViewCellEditControl") != nil {
                $0.isHidden = true
            }
                if $0.description.range(of: "UITableViewCellReorderControl") != nil {
                    let resizedGripView = UIView(frame: CGRect(x: 0, y: 0, width: $0.frame.maxX, height: $0.frame.maxY))
                    resizedGripView.addSubview($0)
                    cell.addSubview(resizedGripView)
                    let transform = CGAffineTransform(translationX: $0.frame.size.width - cell.frame.size.width + 30,
                                                      y: 1)
                    resizedGripView.transform = transform
                }
            }
    }
}
