//
//  ViewController.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 28/12/22.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let homeVC = UINavigationController(rootViewController: HomeViewController())
        homeVC.tabBarItem.image = UIImage(systemName: "house")
        homeVC.title = "Home"

        let upcomingVC = UINavigationController(rootViewController: UpcomingViewController())
        upcomingVC.tabBarItem.image = UIImage(systemName: "play.circle")
        upcomingVC.title = "Coming soon"

        let searchVC = UINavigationController(rootViewController: SearchViewController())
        searchVC.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        searchVC.title = "Top Searches"

        let downloadVC = UINavigationController(rootViewController: DownloadsViewController())
        downloadVC.tabBarItem.image = UIImage(systemName: "arrow.down.to.line.compact")
        downloadVC.title = "Downloads"

        tabBar.tintColor = .label

        let vcList = [homeVC, upcomingVC, searchVC, downloadVC]
        setViewControllers(vcList, animated: true)
    }
}
