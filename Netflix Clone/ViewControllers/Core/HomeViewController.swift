//
//  HomeViewController.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 07/01/23.
//

import UIKit

// MARK: - Enum Sections
enum Sections: Int {
    case Trendingmoview = 0
    case TrendingTV = 1
    case Popular = 2
    case UpcomingMovies = 3
    case TopRated = 4
}

class HomeViewController: UIViewController {
    // MARK: - Properties
    private let homeViewModel = HomeViewViewModel()
    private var headerView: HeroHeaderUIView?
    private let sectionTitles = ["Trending movies", "Trending Tv", "Popular", "Upcoming movies", "Top Rated"]

    private let homeFeed: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(CollectionViewTableViewCell.self,
                           forCellReuseIdentifier: CollectionViewTableViewCell.identifier)
        return tableView
    }()

    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(homeFeed)
        homeFeed.delegate = self
        homeFeed.dataSource = self

        homeFeed.tableHeaderView = getTableHeaderView()
        configureNavBar()
        configureHeroHeaderview()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        homeFeed.frame = view.bounds
    }

    private func getTableHeaderView() -> UIView {
        let tableHeaderView = UIView()
        tableHeaderView.translatesAutoresizingMaskIntoConstraints = false

        headerView = HeroHeaderUIView(frame: CGRect(x: 0, y: 0, width: view.frame.width - 400, height: 600))
        guard let headerView = headerView else { return tableHeaderView }

        tableHeaderView.addSubview(headerView)
        headerView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            headerView.leadingAnchor.constraint(equalTo: tableHeaderView.leadingAnchor, constant: 20),
            headerView.trailingAnchor.constraint(equalTo: tableHeaderView.trailingAnchor, constant: -20),
            headerView.topAnchor.constraint(equalTo: tableHeaderView.topAnchor, constant: 40),
            headerView.bottomAnchor.constraint(equalTo: tableHeaderView.bottomAnchor, constant: -40),
            tableHeaderView.heightAnchor.constraint(equalToConstant: 600),
            tableHeaderView.widthAnchor.constraint(equalToConstant: view.frame.width)
        ])

        return tableHeaderView
    }

    func configureHeroHeaderview() {
        homeViewModel.configureHeroHeaderview { [weak self] result in
            switch result {
            case .success(let (randonTitle, titleViewModel)):
                self?.headerView?.configure(with: titleViewModel, trendingMoview: randonTitle)
                self?.headerView?.delegateTableView = self
                self?.headerView?.delegateVideoDownload = self
                DispatchQueue.main.async {
                    self?.homeFeed.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CollectionViewTableViewCell.identifier,
                                                       for: indexPath) as? CollectionViewTableViewCell else {
            return UITableViewCell()
        }
        cell.delegateTV = self
        cell.delegateDownloadVideo = self
        switch indexPath.section {
        case Sections.Trendingmoview.rawValue:
            homeViewModel.getTrendingMovies { [weak self] result in
                self?.configureCells(with: result, for: cell)
            }
        case Sections.TrendingTV.rawValue:
            homeViewModel.getTrendingTV { [weak self] result in
                self?.configureCells(with: result, for: cell)
            }
        case Sections.Popular.rawValue:
            homeViewModel.getPopularMovies { [weak self] result in
                self?.configureCells(with: result, for: cell)
            }
        case Sections.UpcomingMovies.rawValue:
            homeViewModel.getUpcomingMovies { [weak self] result in
                self?.configureCells(with: result, for: cell)
            }
        case Sections.TopRated.rawValue:
            homeViewModel.getTopRatedMovies { [weak self] result in
                self?.configureCells(with: result, for: cell)
            }
        default:
            return UITableViewCell()
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    private func configureCells(with result: Result<[Title], Error>, for cell: CollectionViewTableViewCell) {
        switch result {
        case .success(let title):
            cell.configure(with: title)
        case .failure(let error):
            print(error)
        }
    }

    private func configureNavBar() {
        let label = UILabel()
        label.text = "For shweta1talmale"
        label.textColor = .label
        label.font = .systemFont(ofSize: 21, weight: .semibold)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: label)

        let profileIcon = UIButton()
        profileIcon.setImage(UIImage(named: "netflixLogo"), for: .normal)
        profileIcon.heightAnchor.constraint(equalToConstant: 25).isActive = true
        profileIcon.widthAnchor.constraint(equalToConstant: 25).isActive = true

        let searchIcon = UIButton()
        searchIcon.setImage(UIImage(systemName: "magnifyingglass"), for: .normal)
        searchIcon.addTarget(self, action: #selector(didTapSearchIcon), for: .touchUpInside)
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(customView: profileIcon),
            UIBarButtonItem(customView: searchIcon)
        ]
        navigationController?.navigationBar.tintColor = .white
    }

    @objc private func didTapSearchIcon() {
        let searchVC = SearchViewController()
        navigationController?.pushViewController(searchVC, animated: true)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let defaultOffset = view.safeAreaInsets.top
        let offset = scrollView.contentOffset.y + defaultOffset

        navigationController?.navigationBar.transform = .init(translationX: 0, y: min(0, -offset))
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section].lowercased()
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let headerView = view as? UITableViewHeaderFooterView else { return }
        headerView.textLabel?.textColor = .label
        headerView.textLabel?.font = .systemFont(ofSize: 18, weight: .semibold)
        headerView.textLabel?.frame = CGRect(x: headerView.bounds.origin.x + 20,
                                             y: headerView.bounds.origin.y,
                                             width: 100,
                                             height: headerView.bounds.height)
        headerView.textLabel?.text = headerView.textLabel?.text?.capitaliseFirstLetter()
    }
}
