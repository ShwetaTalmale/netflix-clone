//
//  LaunchScreenViewController.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 09/02/23.
//

import UIKit

class LaunchScreenViewController: UIViewController {
    // MARK: - Properties
    private let logoImageView: UIImageView = {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 400))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(logoImageView)
        logoImageView.image = UIImage.gifImageWithName("gif")

        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            let mainVC = MainTabBarViewController()
            mainVC.modalPresentationStyle = .fullScreen
            self.present(mainVC, animated: true)
        })
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        logoImageView.center = view.center
    }

}
