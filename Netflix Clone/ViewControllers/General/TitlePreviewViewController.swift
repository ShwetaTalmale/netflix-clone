//
//  TitlePreviewViewController.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 11/01/23.
//

import UIKit
import WebKit

class TitlePreviewViewController: UIViewController {
    // MARK: - Properties
    private let titleLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 22, weight: .bold)
        return label
    }()

    private let overViewLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 18, weight: .regular)
        label.numberOfLines = 0
        return label
    }()

    private let downloadButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .red
        button.setTitle("Download", for: .normal)
        button.setTitleColor(.label, for: .normal)
        button.layer.cornerRadius = 8
        button.layer.masksToBounds = true
        return button
    }()

    private let webView: WKWebView = {
        let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private var movieTitle: Title?
    weak var downloadDelegate: DownloadVideoDelegate?
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(webView)
        view.addSubview(titleLabel)
        view.addSubview(overViewLabel)
        view.addSubview(downloadButton)
        applyConstraints()
        downloadDelegate = self
        downloadButton.addTarget(self, action: #selector(didTapDownloadButton), for: .touchUpInside)
    }

    private func applyConstraints() {
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 120),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            webView.heightAnchor.constraint(equalToConstant: 250),
            titleLabel.topAnchor.constraint(equalTo: webView.bottomAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            overViewLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            overViewLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            overViewLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            downloadButton.topAnchor.constraint(equalTo: overViewLabel.bottomAnchor, constant: 20),
            downloadButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            downloadButton.widthAnchor.constraint(equalToConstant: 140),
            downloadButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }

    @objc private func didTapDownloadButton() {
        guard let movieTitle = movieTitle else { return }
        downloadDelegate?.downloadVideoWith(title: movieTitle)
    }

    func configure(with model: TitlePreviewModel, title: Title) {
        self.movieTitle = title
        titleLabel.text = model.title
        overViewLabel.text = model.titleOverView
        guard let url = URL(string: "https://www.youtube.com/embed/\(model.youTubeView.id.videoId)") else { return }
        webView.load(URLRequest(url: url))
    }
}
