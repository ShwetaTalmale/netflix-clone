//
//  Extensions.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 09/01/23.
//

import Foundation
import UIKit

extension String {
    func capitaliseFirstLetter() -> String {
        return self.prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}

extension UIImageView {
    func makeGradient() {
            let gradient = CAGradientLayer()
            gradient.frame = self.bounds
            gradient.contents = self.image?.cgImage
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
            gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 1)
            self.layer.addSublayer(gradient)
        }
}

extension UIViewController {

func showToast(message: String, font: UIFont) {

    let toastLabel = UILabel()
    toastLabel.translatesAutoresizingMaskIntoConstraints = false
    toastLabel.numberOfLines = 0
    toastLabel.backgroundColor = UIColor.gray.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = font
    toastLabel.textAlignment = .center
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10
    toastLabel.clipsToBounds  =  true
    toastLabel.layoutSubviews()
    self.view.addSubview(toastLabel)
    let labelWidth = toastLabel.intrinsicContentSize.width + 20

    NSLayoutConstraint.activate([
        toastLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        toastLabel.widthAnchor.constraint(equalToConstant: labelWidth),
        toastLabel.heightAnchor.constraint(equalToConstant: 50),
        toastLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,
                                           constant: -100)
    ])

    UIView.animate(withDuration: 10.0, delay: 1, options: .curveEaseIn, animations: {
         toastLabel.alpha = 0.0
    }, completion: { _ in
        toastLabel.removeFromSuperview()
    })
} }

// MARK: - DownloadVideoDelegate
extension UIViewController: DownloadVideoDelegate {
    func downloadVideoWith(title: Title) {
        DataPersistanceManager.shared.downloadTitleWith(model: title) { [weak self] result in
            switch result {
            case .success():
                self?.showToast(message: "Downloading \(title.title ?? title.original_title ?? "Unknown")",
                                font: .systemFont(ofSize: 15, weight: .semibold))
                NotificationCenter.default.post(name: NSNotification.Name("Downloaded"), object: nil)
            case .failure(let error):
                print(error)
            }
        }
    }
}

// MARK: - CollectionViewTableViewCellDelegate
extension UIViewController: CollectionViewTableViewCellDelegate {
    func collectionViewTableViewCellDidTapCell(_ cell: CollectionViewTableViewCell?,
                                               model: TitlePreviewModel,
                                               title: Title) {
        DispatchQueue.main.async {
            let vc = TitlePreviewViewController()
            vc.configure(with: model, title: title)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
