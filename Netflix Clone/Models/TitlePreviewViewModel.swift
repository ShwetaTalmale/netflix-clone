//
//  TitlePreviewModel.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 11/01/23.
//

import Foundation

struct TitlePreviewModel {
    let title: String
    let youTubeView: VideoElement
    let titleOverView: String
}
