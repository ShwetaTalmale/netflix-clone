//
//  TrendingMoviesResponse.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 09/01/23.
//

import Foundation

struct TrendingTitleResponse: Codable {
    let results: [Title]
}

struct Title: Codable {
    let id: Int
    let media_type: String?
    let original_language: String?
    let original_title: String?
    let overview: String?
    let popularity: CGFloat
    let poster_path: String?
    let release_date: String?
    let title: String?
    let vote_count: Int
}
