//
//  titleViewModel.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 10/01/23.
//

import Foundation

struct TitleViewModel {
    let titleName: String
    let posterUrl: String
}
