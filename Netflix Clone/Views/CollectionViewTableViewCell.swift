//
//  CollectionViewTableViewCell.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 07/01/23.
//

import UIKit

protocol CollectionViewTableViewCellDelegate: AnyObject {
    func collectionViewTableViewCellDidTapCell(_ cell: CollectionViewTableViewCell?,
                                               model: TitlePreviewModel,
                                               title: Title)
}
protocol DownloadVideoDelegate: AnyObject {
    func downloadVideoWith(title: Title)
}

class CollectionViewTableViewCell: UITableViewCell {
    // MARK: - Properties
    static let identifier = "CollectionViewTableViewCell"
    private var titles: [Title] = [Title]()
    weak var delegateTV: CollectionViewTableViewCellDelegate?
    weak var delegateDownloadVideo: DownloadVideoDelegate?

    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 140, height: 200)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(TitleCollectionViewCell.self,
                                forCellWithReuseIdentifier: TitleCollectionViewCell.identifier)
        return collectionView
    }()

    // MARK: - Life cycle Methods
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .systemPink
        contentView.addSubview(collectionView)

        collectionView.dataSource = self
        collectionView.delegate = self
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = contentView.bounds
    }

    public func configure(with titles: [Title]) {
        self.titles = titles
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension CollectionViewTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TitleCollectionViewCell.identifier,
                                                            for: indexPath) as? TitleCollectionViewCell,
              let poster_path = titles[indexPath.row].poster_path else { return UICollectionViewCell() }
        cell.configure(with: poster_path)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)

        let title = titles[indexPath.row]
        guard let titleName = title.title ?? title.original_title else { return }

        APICaller.shared.getMovie(with: titleName + " trailer") { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let videoElement):
                let title = self.titles[indexPath.row]
                let model = TitlePreviewModel(title: title.title ?? title.original_title ?? "Unknown",
                                                  youTubeView: videoElement,
                                                  titleOverView: title.overview ?? "Unknown")
                self.delegateTV?.collectionViewTableViewCellDidTapCell(self, model: model, title: title)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    // MARK: - Download Video
    func collectionView(_ collectionView: UICollectionView,
                        contextMenuConfigurationForItemAt indexPath: IndexPath,
                        point: CGPoint) -> UIContextMenuConfiguration? {
        let config = UIContextMenuConfiguration(actionProvider: { [weak self] _ in
            let downloadAction = UIAction(title: "Download", state: .off) { _ in
               // self?.downloadTitleAt(indexpath: indexPath)
                guard let title = self?.titles[indexPath.row] else { return }
                self?.delegateDownloadVideo?.downloadVideoWith(title: title)
            }
            return UIMenu(title: "", image: nil, identifier: nil, options: .displayInline, children: [downloadAction])
        })
        return config
    }
}
