//
//  HeroHeaderUIView.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 07/01/23.
//

import UIKit
import SDWebImage

class HeroHeaderUIView: UIView {
    // MARK: - Properties
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 15
        stackView.distribution = .fillEqually
        return stackView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.font = .systemFont(ofSize: 12, weight: .semibold)
        label.textColor = .white
        return label
    }()

    private let downloadButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "plus"), for: .normal)
        button.tintColor = .white
        button.setTitle("Download", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: .bold)
        button.backgroundColor = .darkGray
        button.setTitleColor(.white, for: .normal)
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 0
        button.layer.cornerRadius = 5
        return button
    }()
    private let playButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "play.fill"), for: .normal)
        button.tintColor = .black
        button.setTitle("Play", for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: .bold)
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
        button.contentMode = .center
        return button
    }()
    private let heroImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    private var randomTrendingMovie: Title?
    weak var delegateTableView: CollectionViewTableViewCellDelegate?
    weak var delegateVideoDownload: DownloadVideoDelegate?

    // MARK: - Life cycle methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 1
        layer.cornerRadius = 10
        layer.borderColor = UIColor.darkGray.cgColor
        applyConstraints()
        playButton.addTarget(self, action: #selector(didTapPlayButton), for: .touchUpInside)
        downloadButton.addTarget(self, action: #selector(didTapDownloadButton), for: .touchUpInside)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        heroImageView.frame = bounds
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func addGradient() {
        let view = UIView(frame: bounds)
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [
            UIColor.clear.cgColor,
            UIColor.black.cgColor
        ]
        gradientLayer.frame = bounds
        gradientLayer.locations = [0.0, 1.0]
        view.layer.insertSublayer(gradientLayer, at: 0)
        heroImageView.addSubview(view)
        heroImageView.bringSubviewToFront(view)
    }

    private func applyConstraints() {
        addSubview(heroImageView)
        addSubview(titleLabel)
        addSubview(buttonStackView)
        buttonStackView.addArrangedSubview(playButton)
        buttonStackView.addArrangedSubview(downloadButton)

        NSLayoutConstraint.activate([
            buttonStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            buttonStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            buttonStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            playButton.heightAnchor.constraint(equalToConstant: 35),
            downloadButton.heightAnchor.constraint(equalToConstant: 35),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: playButton.topAnchor, constant: -10)
        ])
    }

    @objc private func didTapDownloadButton() {
        guard let title = randomTrendingMovie else { return }
        delegateVideoDownload?.downloadVideoWith(title: title)
    }

    @objc private func didTapPlayButton() {
        guard let title = randomTrendingMovie else { return }
        APICaller.shared.getMovie(with: title.title ?? "Unknown") { [weak self] result in
            switch result {
            case .success(let videoElement):
                DispatchQueue.main.async {
                    let titlePreviewModel =
                    TitlePreviewModel(title: title.title ?? title.original_title ?? "Unknown",
                                          youTubeView: videoElement,
                                          titleOverView: title.overview ?? "Unknown")
                    self?.delegateTableView?.collectionViewTableViewCellDidTapCell(nil, model: titlePreviewModel,
                                                                                   title: title)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func configure(with model: TitleViewModel, trendingMoview: Title?) {
        guard let url = URL(string: "https://image.tmdb.org/t/p/w500/\(model.posterUrl)") else { return }
        heroImageView.sd_setImage(with: url)
        randomTrendingMovie = trendingMoview
        DispatchQueue.main.async { [weak self] in
            self?.heroImageView.makeGradient()
            self?.titleLabel.text = trendingMoview?.title ?? trendingMoview?.original_title ?? "Unknown"
        }
    }
}
