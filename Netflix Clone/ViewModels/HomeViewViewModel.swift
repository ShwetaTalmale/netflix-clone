//
//  HomeViewViewModel.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 21/02/23.
//

import Foundation

enum HomeViewError: Error {
    case failedToGetHeroHeaderView
}

final class HomeViewViewModel {
    func configureHeroHeaderview(completion: @escaping (Result<(randonTitle: Title,
                                                                titleViewModel: TitleViewModel),
                                                        Error>) -> Void) {
        APICaller.shared.getTrendingMovies { result in
            switch result {
            case .success(let title):
                guard let randomTitle = title.randomElement() else {
                    completion(.failure(HomeViewError.failedToGetHeroHeaderView))
                    return
                }
                completion(.success((randomTitle,
                                     TitleViewModel(titleName: randomTitle.title ??
                                                    randomTitle.original_title ?? "Unknown",
                                                    posterUrl: randomTitle.poster_path ?? ""))))

            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func getTrendingMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.getTrendingMovies { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getTrendingTV(completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.getTrendingTV { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getPopularMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.getPopularMovies { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getUpcomingMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.getUpcomingMovies { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getTopRatedMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.getTopRatedMovies { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getMovie(with query: String, completion: @escaping (Result<VideoElement, Error>) -> Void) {
        APICaller.shared.getMovie(with: query) { result in
            switch result {
            case .success(let videoElement):
                completion(.success(videoElement))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getDiscoverableMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.getDiscoverableMovies { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func searchMovie(with query: String, completion: @escaping (Result<[Title], Error>) -> Void) {
        APICaller.shared.search(with: query) { result in
            switch result {
            case .success(let titles):
                completion(.success(titles))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
