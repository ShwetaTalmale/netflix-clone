//
//  APICaller.swift
//  Netflix Clone
//
//  Created by Shweta Talmale on 09/01/23.
//

import Foundation

struct Constants {
    static var API_KEY = "30134ccf8fdbd427cc3e664003f5c893"
    static var baseURL = "https://api.themoviedb.org"
    static let youtubeApi_key = "AIzaSyBIefXdf8QbTIII65Y3wN8bCeNH_hlbyjE"
    static let youtubeBaseUrl = "https://youtube.googleapis.com/youtube/v3/search?"
}

enum APIError {
    case failedToGetData
}

class APICaller {
    static let shared = APICaller()
    func getTrendingMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        let urlString = "\(Constants.baseURL)/3/trending/movie/day?api_key=\(Constants.API_KEY)"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }
    func getTrendingTV(completion: @escaping (Result<[Title], Error>) -> Void) {
        let urlString = "\(Constants.baseURL)/3/trending/tv/day?api_key=\(Constants.API_KEY)"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }

    func getPopularMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        let urlString = "\(Constants.baseURL)/3/movie/popular?api_key=\(Constants.API_KEY)&language=en-US&page=1"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }

    func getUpcomingMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        let urlString = "\(Constants.baseURL)/3/movie/upcoming?api_key=\(Constants.API_KEY)&language=en-US&page=1"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }

    func getTopRatedMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        let urlString = "\(Constants.baseURL)/3/movie/top_rated?api_key=\(Constants.API_KEY)"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }

    // swiftlint:disable line_length
    func getDiscoverableMovies(completion: @escaping (Result<[Title], Error>) -> Void) {
        let urlString = "\(Constants.baseURL)/3/discover/movie?api_key=\(Constants.API_KEY)&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_watch_monetization_types=flatrate"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }

    func search(with query: String, completion: @escaping (Result<[Title], Error>) -> Void) {
        guard let query = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }
        let urlString = "\(Constants.baseURL)/3/search/movie?api_key=\(Constants.API_KEY)&query=\(query)"
        guard let url = URL(string: urlString) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(TrendingTitleResponse.self, from: data)
                completion(.success(results.results))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }

    func getMovie(with query: String, completion: @escaping (Result<VideoElement, Error>) -> Void) {
        guard let query = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else { return }
        let url = "\(Constants.youtubeBaseUrl)q=\(query)&key=\(Constants.youtubeApi_key)"
        guard let utubeUrl = URL(string: url) else { return }

        let task = URLSession.shared.dataTask(with: URLRequest(url: utubeUrl), completionHandler: { data, _, error in
            guard let data = data, error == nil else { return }
            do {
                let results = try JSONDecoder().decode(YoutubeSearchResponse.self, from: data)
                completion(.success(results.items[0]))
            } catch {
                completion(.failure(error))
            }
        })
        task.resume()
    }
}

/*
 https://api.themoviedb.org/3/search/movie?api_key={api_key}&query=Jack+Reacher
 */
