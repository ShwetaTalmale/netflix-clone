//
//  APICallerTest.swift
//  Netflix Clone Tests
//
//  Created by Shweta Talmale on 10/02/23.
//

import XCTest
@testable import Netflix_Clone

final class APICallerTest: XCTestCase {
    var query = ""

    func testTrendingMoviesSuccess() {
        let expectation = self.expectation(description: "Test Trending Movies")
        APICaller.shared.getTrendingMovies { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
                self.query = title.randomElement()?.title ?? "" + "trailer"
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testTrendingMoviesFailure() {
        Constants.API_KEY = "30134ccf8fdbd427cc3e664003f"
        let expectation = self.expectation(description: "Test Trending Movies")
        APICaller.shared.getTrendingMovies { result in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(_):
                break
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 50)
    }

    func testTrendingTV() {
        let expectation = self.expectation(description: "Test Trending TV")
        APICaller.shared.getTrendingTV { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testPopularMovies() {
        let expectation = self.expectation(description: "Test Popular Movies")
        APICaller.shared.getPopularMovies { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testUpcomingMovies() {
        let expectation = self.expectation(description: "Test Trending Movies")
        APICaller.shared.getUpcomingMovies { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testTopRatedMovies() {
        let expectation = self.expectation(description: "Test Trending Movies")
        APICaller.shared.getTopRatedMovies { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testDiscoverableMovies() {
        let expectation = self.expectation(description: "Test Trending Movies")
        APICaller.shared.getDiscoverableMovies { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testSearchMovies() {
        let expectation = self.expectation(description: "Test Trending Movies")
        APICaller.shared.search(with: "harry") { result in
            switch result {
            case .success(let title):
                XCTAssertNotNil(title)
                XCTAssertTrue(title is [Title])
                let trendingMovie = TrendingTitleResponse(results: title)
                XCTAssertNotNil(trendingMovie)
                XCTAssertEqual(trendingMovie.results[0].title, title[0].title)
            case .failure(let error):
                XCTAssertNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testMoviesWithQueryFailure() {
        let expectation = self.expectation(description: "Test Youtube search response")
        APICaller.shared.getMovie(with: "Harry") { result in
            switch result {
            case .success(let title):
                XCTAssertNil(title)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }

    func testMoviesWithQuerySuccess() {
        let expectation = self.expectation(description: "Test Youtube search response")
        APICaller.shared.getMovie(with: query) { result in
            switch result {
            case .success(let videoElement):
                XCTAssertNotNil(videoElement)
                XCTAssertTrue(videoElement is VideoElement)
                let searchResponse = YoutubeSearchResponse(items: [videoElement])
                XCTAssertEqual(searchResponse.items.first?.id.kind, videoElement.id.kind)
                XCTAssertEqual(searchResponse.items.first?.id.videoId, videoElement.id.videoId)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
}
